# Readme

## Todos

1. Check if NonZero and pinning works to wrap the LowDiscrepancySequence inside its own type
    * See [Rust API doc - std::pin - Example self referential struct](https://doc.rust-lang.org/std/pin/index.html#example-self-referential-struct)
2. Check pinning and self referential structs here: [https://arunanshub.hashnode.dev/self-referential-structs-in-rust]
